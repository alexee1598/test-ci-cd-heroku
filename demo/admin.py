from django.contrib import admin

from demo.models import MyModel


@admin.register(MyModel)
class MyModelAdmin(admin.ModelAdmin):
    pass

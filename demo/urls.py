from django.urls import path

from demo import views

urlpatterns = [
    path('test1/', views.demo_multiply_contextmanager, name='demo_multiply_contextmanager'),
    path('test2/', views.demo_strict_zip, name='demo_strict_zip'),
    path('test3/', views.demo_matching, name='demo_matching'),
    path('test4/', views.demo_matching_v2, name='demo_matching_v2'),
    path('test5/', views.demo_matching_class_v1, name='demo_matching_class_v1'),
    path('test6/', views.demo_matching_class_v2, name='demo_matching_class_v2'),
]

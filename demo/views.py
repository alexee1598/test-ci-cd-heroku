import csv
import math
from typing import TypeAlias

from django.views.decorators.cache import cache_page
from rest_framework.decorators import api_view
from rest_framework.response import Response

FromTo: TypeAlias = list[str, str]
ToFromTo: TypeAlias = list[str, str, str]


@api_view(['GET'])
@cache_page(timeout=20)
def demo_multiply_contextmanager(request) -> Response:
    with open('file.csv', 'r+') as first, open('file2.csv', 'r+') as second:
        first_file = csv.reader(first)
        second_file = csv.reader(second)
        mydict = {rows[0]: rows[1] for rows in first_file}
        mydict2 = {rows[0]: rows[1] for rows in second_file}

    return Response({'result': mydict2 | mydict})


@api_view(['GET'])
@cache_page(timeout=20)
def demo_strict_zip(request) -> Response:
    test_array = [1, 2, 3, 4, 5]
    second_array = [2, 4, 5, 7, 8, 9]
    try:
        for i, b in zip(test_array, second_array, strict=True):
            print('i ', i)
            print('b ', b)
    except ValueError:
        return Response({'error': "array don't have the same length"})

    return Response({'result': True})


# instead of Union[FromTo, ToFromTo]
def demo_type_alias(data: FromTo) -> FromTo | ToFromTo:
    return data


@api_view(['GET'])
@cache_page(timeout=20)
def demo_matching(request) -> Response:
    user = {'name': {'first': 'Pablo', 'last': 'Galindo Salgado'}, 'title': 'Python 3.10 release manager', 'age': 22}
    match user:
        case {'name': {'last2': last_name}}:
            return Response({'last_name': last_name})
        case {'age': age}:
            return Response({'age': age})


@api_view(['GET'])
@cache_page(timeout=20)
def demo_matching_v2(request) -> Response:
    user = [2.33, 1, 2, 3, 4]
    match user:
        case (int(first), *_):
            return Response({'last_name': first})
        case (str(_) | bytes(_), *number) if user:
            return Response({'age': sum(number)})
        case _:
            return Response({'error': 'Doesn\'t match any case'})


class Vector:
    __match_args__ = ('x', 'y')

    def __init__(self, x, y):
        self.__x = float(x)
        self.__y = float(y)

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y


@api_view(['GET'])
@cache_page(timeout=20)
def demo_matching_class_v1(request) -> Response:
    v = Vector(1.3, 0)
    match v:
        case Vector(x=0, y=y):
            return Response({'y': y})
        case Vector(x=x, y=0):
            return Response({'age': x})
        case Vector(x=x, y=y):
            return Response({'age': sum([x, y])})
        case _:
            return Response({'error': 'Doesn\'t match any case'})


@api_view(['GET'])
@cache_page(timeout=20)
def demo_matching_class_v2(request) -> Response:
    l = set()
    for n in range(10_000_000):
        l.add(math.pi * n)
    v = Vector(1.3, 2)
    match v:
        case Vector(0, y):
            return Response({'y': y})
        case Vector(x, 0):
            return Response({'x': x})
        case Vector(x, y):
            return Response({'sum_x_y': sum([x, y])})
        case _:
            return Response({'error': 'Doesn\'t match any case'})
